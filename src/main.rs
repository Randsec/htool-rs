use ansi_term::Colour::Yellow;
use std::env;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::PathBuf;
use std::{fs::File, str::from_utf8};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opt {
    /// Size of the output
    #[structopt(name = "size", default_value = "16", long, short)]
    size: u8,

    /// Offset to start reversing
    #[structopt(name = "offset", default_value = "0", long, short)]
    offset: usize,

    /// File to read
    #[structopt(parse(from_os_str))]
    file: PathBuf,
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let opt = Opt::from_args();
    /*if args.len() < 2 {
        //print_help();
        let opt = Opt::from_args();
        println!("{:?}", opt);
        std::process::exit(0);
    }*/
    let file = File::open(&args[1])?;
    let buf_reader = BufReader::new(file);
    let size = opt.size;
    print_top_offset(size);

    let mut iter: u64 = 0;
    let hex_value = format!("{:08x}   ", iter);
    print!("{}", Yellow.paint(hex_value));

    let mut ascii_bytes: Vec<u8> = Vec::new();

    for byte in buf_reader.bytes() {
        let b = byte.unwrap();
        print!("{:02x} ", b);
        if b > 32 && b < 128 {
            ascii_bytes.push(b);
        } else {
            ascii_bytes.push(String::from(".").into_bytes()[0]);
        }

        iter += 1;
        if iter % size as u64 == 0 {
            let c = from_utf8(&ascii_bytes);
            print!(" |  {}", c.unwrap());
            let hex_value = format!("\n{:08x}   ", iter);
            print!("{}", Yellow.paint(hex_value));
            ascii_bytes.clear();
        }
    }

    Ok(())
}

fn print_top_offset(size: u8) {
    print!("{}", Yellow.paint("Offset\t   "));
    for i in 0x0..size {
        let hex_value = format!("{:02x}", i);
        print!("{} ", Yellow.paint(hex_value));
    }
    println!();
}
