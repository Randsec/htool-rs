# htool-rs
Hex tool utility coded in Rust.

## Dependencies
For a full list of dependencies check Cargo.toml

## Compile from source
- Clone the repository
- Run: `cargo build --release`
- ez